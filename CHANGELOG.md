# Releases

## [0.6.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.6.0) - 04.04.2024
### Added
- direct contraction of sparse arrays of `MatrixSemiring` element.
### Changed
- `sort` is applicable to any size array

## [0.5.5](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.5) - 04.04.2024
### Fixed
- transpose of a sparse matrix of matrix semiring element gives incorrect
  results: the method is not properly dispatched

## [0.5.4](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.4) - 04.04.2024
### Fixed
- `zero` of matrix semiring throw an error

## [0.5.3](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.3) - 03.04.2024
### Improved
- type stability of operations on `MatrixSemiring` elements
### Fixed
- `vcat` of sparse vectors return inconsistent sparse array
- broadcasting of direct sum on `MatrixSemiring`
- constructor `sparse` fails when no coordinates and no size are provided ->
  now returns a `0`-dimensional tensor

## [0.5.2](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.2) - 03.04.2024
### Fixed
- constructor `sparse` fails when no coordinates and no size are provided ->
  now returns a `0`-dimensional tensor

## [0.5.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.1) - 03.04.2024
### Fixed
- outer product return a matrix with the wrong size

## [0.5.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.5.0) - 02.04.2024
### Added
- sparse arrays implement `permutedims`
- specialized broadcast operation for direct sum on array of matrix semiring
  element.
### Changed
- Removed the `DirectMatrixSemiring` and added a direct sum and direct product
  operator for the matrix semring

## [0.4.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.4.0) - 29.03.2024
### Added
- Support for any dimension array not just vectors and matrices.
- matrix semirings `MatrixSemiring` (natural matrix semiring) and
 `DirectMatrixSemiring` using the direct product and direct sum.
### Changed
- overload `Base.cat` rather than `Base.vcat`.

## [0.3.2](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.3.2) - 27.03.2024
### Fixed
- blockdiag return a dense matrix

## [0.3.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.3.1) - 27.03.2024
### Fixed
- preserve the type of the return value when broadcasting with `blocdiag.`

## [0.3.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.3.0) - 27.03.2024
### Changed
- API closer to [SparseArrays.jl](https://github.com/JuliaSparse/SparseArrays.jl)
- size of the sparse arrays is contained in the type parameters
- multiplying a sparse array with `zero(T)` will return an empty sparse array
### Added
- `Base.zero` for sparse array
- `similar` and `fill!` for sparsearrays

## [0.2.1](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.2.1) - 25.03.2024
### Added
- possibility to specify a specific value as zero element for spare matrices
- broadcasted version of `blockdiag` and `transpose` on spare array of sparse
  matrices

## [0.2.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.2.0) - 21.03.2024
### Added
- support for dense vectors/matrices of semiring elements
- computing the nullspace of a matrix of semring elements
- matrix - matrix multiplication (dense and sparse)


## [0.1.0](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl/-/tags/v0.1.0) - 20.03.2024
Forked of [SumSparseTensors.jl]https://gitlab.lisn.upsaclay.fr/fast/fst/sumsparsetensors.jl)
The package now implements only sparse vector / matrix (no arbitrary size
tensors)


