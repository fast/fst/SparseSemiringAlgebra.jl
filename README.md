# SparseSemiringAlgebra.jl

This package is implement basic manipulation of sparse vectors/matrices of
semiring elements. It is algorithmic core of the [TensorFSTs.jl](https://gitlab.lisn.upsaclay.fr/fast/TensorFSTs.jl)
package.

