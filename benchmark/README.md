# Benchmark

To run in the `benchmakr/`
```bash
$ julia --project runbenchmark.jl [-d OUT_DIR] [-r GIT_REV]
```

By default, the output files are saved in the current directory and the `HEAD`
is used for the revision.

