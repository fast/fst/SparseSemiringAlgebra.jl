
using ArgParse
using BenchmarkTools
using InteractiveUtils
using Pkg
using Random


s = ArgParseSettings()
@add_arg_table s begin
    "--dir", "-d"
        arg_type = String
        default = "./"
        help = "output directory (default: \"./\")"
    "--no-test"
        action = :store_true
        help = "disable testing the package before running the benchmark"
    "--rev", "-r"
        arg_type = String
        default = "HEAD"
        help = "git repository revision to use (default: HEAD)"

end

args = parse_args(s)

# Retrieve the rev id.
gitrev = strip(read(`git rev-parse $(args["rev"])`, String))

# Create a temporary Julia environment.
Pkg.activate(; temp = true)
Pkg.add(name = "SparseSemiringAlgebra", path = "..", rev = gitrev)
Pkg.add("Semirings")


# Make sure we are benchmarking a working version.
if ! args["no-test"]
    Pkg.test(name = "SparseSemiringAlgebra", path = "..", rev = gitrev)
end


#=============================================================================#
# Benchmark


settings = Dict(
    "seed" => 123,
    "num_elements" => 1_000_000,
    "ndims" => 4,
    "sort_dim" => 1,
    "sort_dim_length" => 100_000,
    "type_value" => Float32,
    "type_index" => Int
)


Random.seed!(settings["seed"])


using Semirings
using SparseSemiringAlgebra
const SSA = SparseSemiringAlgebra

bench = @benchmarkset "SparseSemiringAlgebra" begin
    N = settings["ndims"]
    numel = settings["num_elements"]
    len = settings["sort_dim_length"]
    sdim = settings["sort_dim"]
    T = settings["type_value"]
    Ti = settings["type_index"]

    @benchmarkset "base" begin
        coordinates = Ti.(rand(1:len, numel, N))
        values = T.(randn(numel))

        #@case "_sort" SSA._sort($coordinates, $values, $sdim, $len)

        out = similar(coordinates, 2*length(coordinates), N)
        xsize = ntuple(n -> len, N)
        @case "_cat!" SSA._cat!($out, $coordinates, $coordinates, $xsize)

        out = similar(coordinates, numel, N-1)
        @case "_dropdims!" SSA._dropdims!($out, $coordinates, (1,))

        out = similar(coordinates)
        @case "_permutedims!" SSA._permutedims!($out, $coordinates, (4, 1, 3, 2))

        out = similar(coordinates)
        @case "_sum!" SSA._sum!($out, $coordinates, (1,))
    end

    @benchmarkset "linalg" begin
        coo2d = Ti.(rand(1:len, numel, 2))
        coo1d = Ti.(rand(1:len, numel, 1))
        coo1d_2 = Ti.(rand(1:len, numel, 1))
        values = T.(randn(numel))

        out = similar(coo2d)
        @case "_transpose_mat!" SSA._transpose_mat!($out, $coo2d)

        out = similar(coo1d)
        @case "_transpose_vec!" SSA._transpose_vec!($out, $coo1d)

        out = similar(values)
        @case "_scalarmul!" SSA._scalarmul!($out, (u, v) -> u * v, $T(3.4), $values)

        dv = T.(randn(len))
        @case "_dot_dvspv" SSA._dot_dvspv((u, v) -> u * v, $dv, $coo1d, $values)

        buffer = similar(values, len)
        @case "_dot_spvspv" SSA._dot_spvspv($buffer, $coo1d, $values, $coo1d_2, $values, $len)

        d = floor(Int, sqrt(numel))
        xcoo = Ti.(rand(1:len, d, 1))
        xval = T.(randn(d))
        ycoo = Ti.(rand(1:len, d, 1))
        yval = T.(randn(d))
        zcoo = similar(xcoo, d*d, 2)
        zval = similar(xval, d*d)
        @case "_outer_spvspv!" SSA._outer_spvspv!($zcoo, $zval, $xcoo, $xval, $ycoo, $yval)

        x = T.(randn(numel))
        ycoo = Ti.(rand(1:len, numel, 1))
        yval = T.(randn(numel))
        out = similar(x)
        @case "_add_dtspt!" SSA._add_dtspt!($out, $x, $ycoo, $yval)

        d = floor(Int, sqrt(numel))
        Acoo = Ti.(rand(1:d, numel, 2))
        Aval = T.(randn(numel))
        A = sort(SSA.SparseMatrix{T,Ti}((d, d), Acoo, Aval), 1)
        Aptr = A.ptr
        x = T.(randn(d))
        out = similar(x)
        @case "_mvmul_spmdv!" SSA._mvmul_spmdv!($out, (u, v) -> u*v, $Aptr, $Acoo, $Aval, $x, 2)

        d = floor(Int, sqrt(numel))
        xcoo = Ti.(rand(1:d, d, 2))
        xval = T.(randn(d))
        ycoo = Ti.(rand(1:d, d, 2))
        yval = T.(randn(d))
        ysize = (d, d)
        zcoo = similar(xcoo, d*d, 2)
        zval = similar(xval, d*d)
        @case "_kron!" SSA._kron!($zcoo, $zval, $xcoo, $xval, $ycoo, $yval, $ysize)
    end
end

# Execute the benchmark.
results = run(bench, verbose = true)

#=============================================================================#

dirname = args["dir"]

# Dump the raw results. They can be loaded back with
# `BenchmarkTools.load(".../results.json")`
mkpath(dirname)
BenchmarkTools.save(joinpath(dirname, "results.json"), results)


# Write the summary
open(joinpath(dirname, "summary.md"), "w") do f
    println(f, "# Benchmark Summary")
    println(f)

    println(f, "## Git revision")
    println(f, "```bash")
    println(f, "\$ git rev-parse $(args["rev"])")
    println(f, "$gitrev")
    description = strip(read(`git describe $(args["rev"])`, String))
    println(f, "\$ git describe $(args["rev"])")
    println(f, "$description")
    println(f, "```")
    println(f)

    println(f, "## System information")
    println(f, "```julia")
    println(f, "julia> versioninfo()")
    versioninfo(f)
    println(f, "```")
    println(f)

    println(f, "## Benchmark settings")
    println(f, "| parameter | value |")
    println(f, "|:---------:|:-----:|")
    for k in sort(collect(keys(settings)))
        v = settings[k]
        println(f, "| ", k, " | ", v, "|")
    end
    println(f)

    println(f, "## Benchark results")
    for mod in ["base", "linalg"]
        println(f, "### `$mod` module")
        println(f, "| Function | min. time (ms) | # allocs |")
        println(f, "|:---------|---------------:|---------:|")
        for k in sort(collect(keys(results["SparseSemiringAlgebra"][mod])))
            v = results["SparseSemiringAlgebra"][mod][k]
            mv = minimum(v)
            println(f, "| ", k, " | ", mv.time * 1e-6, " | ", mv.allocs, " |")
        end
        println(f)
    end
end

