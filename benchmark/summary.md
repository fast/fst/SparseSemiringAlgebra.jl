# Benchmark Summary

## Git revision
```bash
$ git rev-parse HEAD
2f5b6d1b41c4a3d25f8d1100b29c9428d8ce1ab4
$ git describe HEAD
v0.6.0-60-g2f5b6d1
```

## System information
```julia
julia> versioninfo()
Julia Version 1.10.2
Commit bd47eca2c8a (2024-03-01 10:14 UTC)
Build Info:
  Official https://julialang.org/ release
Platform Info:
  OS: Linux (x86_64-linux-gnu)
  CPU: 8 × 11th Gen Intel(R) Core(TM) i5-1145G7 @ 2.60GHz
  WORD_SIZE: 64
  LIBM: libopenlibm
  LLVM: libLLVM-15.0.7 (ORCJIT, tigerlake)
Threads: 1 default, 0 interactive, 1 GC (on 8 virtual cores)
Environment:
  LD_LIBRARY_PATH = /snap/nvim/2819/usr/lib:/snap/nvim/2819/usr/lib/x86_64-linux-gnu:/var/lib/snapd/lib/gl:/var/lib/snapd/lib/gl32:/var/lib/snapd/void
```

## Benchmark settings
| parameter | value |
|:---------:|:-----:|
| ndims | 4|
| num_elements | 1000000|
| seed | 123|
| sort_dim | 1|
| sort_dim_length | 100000|
| type_index | Int64|
| type_value | Float32|

## Benchark results
### `base` module
| Function | min. time (ms) | # allocs |
|:---------|---------------:|---------:|
| _cat! | 6.730627999999999 | 0 |
| _dropdims! | 2.354057 | 0 |
| _permutedims! | 3.340147 | 0 |
| _sum! | 3.07627 | 0 |

### `linalg` module
| Function | min. time (ms) | # allocs |
|:---------|---------------:|---------:|
| _add_dtspt! | 1.259115 | 0 |
| _dot_dvspv | 0.9779589999999999 | 0 |
| _dot_spvspv | 2.040149 | 0 |
| _kron! | 1.354191 | 0 |
| _mvmul_spmdv! | 0.944742 | 0 |
| _outer_spvspv! | 1.155289 | 0 |
| _scalarmul! | 0.150505 | 0 |
| _transpose_mat! | 1.3514389999999998 | 0 |
| _transpose_vec! | 1.110266 | 0 |

