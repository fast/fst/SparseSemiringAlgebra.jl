
push!(LOAD_PATH, "..")

using Documenter
using SumSparseTensors

makedocs(
    sitename = "SparseSemiringAlgebra.jl",
    repo = Remotes.GitLab("gitlab.lisn.upsaclay.fr", "fast/fst", "SparseSemiringAlgebra.jl"),
    format = Documenter.HTML(prettyurls = false),
    pages = [
        "Home" => "index.md",
        "Sparse Arrays" => "sparray.md",
    ],
    modules = [SparseSemiringAlgebra]
)
