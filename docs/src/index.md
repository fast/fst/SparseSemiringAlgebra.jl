# Sparse Semiring Algebra

The [SparseSemiringAlgebra.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/SparseSemiringAlgebra.jl)
is a Julia package for manipulating sparse arrays whose elements are values
in a semiring. It is the algorithmic core of the
[TensorFSTs.jl package ](https://gitlab.lisn.upsaclay.fr/fast/fst/TensorFSTs.jl).

## Installation

This package is part of the [FAST](https://gitlab.lisn.upsaclay.fr/fast) tool
collection  and requires the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to be installed. To install the registry type:
```julia
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
```
in the package mode of the Julia REPL. You can then install
SparseSemiringAlgebra.jl package with the command
```
pkg> add SparseSemiringAlgebra
```

## Contact

If you have any questions please contact
[Lucas ONDEL YANG](mailto:lucas.ondel-yang@lisn.upsaclay.fr).

