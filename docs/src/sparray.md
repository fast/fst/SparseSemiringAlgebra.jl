# Sparse Arrays

Sparse arrays are arrays that store only the values different from zero
(as defined by the type). The SparseSemiringAlgebra.jl package provides a
programming interface to manipulate such arrays.

## Generic Sparse Array

By default, SparseSemiringAlgebra.jl store sparse arrays in [coordinate
format](https://en.wikipedia.org/wiki/Sparse_matrix#Coordinate_list_(COO)) via
the [`SparseArray{Tv,Ti,N}`](@ref) type. Sparse arrays are created with
the [`sparse`](@ref) function:

```julia-repl
julia> sparse([[1, 2
```

An empty sparse array can be created with the [`spzeros`](@ref) function
```julia-repl
julia> spzeros(Float32, Int32, 3, 3, 4)
```

## Array of Sparse Arrays

SparseSemiringAlgebra.jl provides a special container to manipulate list
of sparse arrays: [`VecSparseArray`](@ref). This type is meant dedicated
for fast broacasted operations on list of sparse arrays. Vector of sparse
are created with the [`vsparse`](@ref)
```julia-repl

```

## Sparse Array of Sparse Arrays

In some situation we would like to have a nested sparse array i.e. a sparse
array of sparse arrays (of sparse arrays etc...) but this is not
possible in general as the notion of zero is not defined for arbitrary arrays.
However, there is a particular case of interest: square matrices. Indeed,
the set of square matrices form a semiring and therefore have the notion
of zero.

SparseSemiringAlgebra.jl can has a special type that represents square matrices
of size `DxD`
```julia-repl

```

With this type, it is possible to have a sparse arrays of square (sparse)
matrix.
```julia-repl

```

It is also possible (and recommended) to use a [`VecSparseArray`](@ref) to
efficiently represent the non-zero matrices:
```julia-repl

```

