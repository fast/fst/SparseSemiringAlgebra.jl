# SPDX-License-Identifier: CECILL-2.1

module SparseSemiringAlgebra

using ChainRulesCore
using LinearAlgebra
using Semirings


# Constructors
export sparsevec,
       spzeros


# Sparse vector / matrix API
export findnz,
       nnz,
       nonzeros


include("abstractsparse.jl")
include("sparsevector.jl")

#include("sparsearray.jl")

#include("sort.jl")

#export collapse
#
#include("base.jl")
#include("linalg.jl")

#export MatrixSemiring, ⊕, ⊗

#include("matrixsemiring.jl")

end

