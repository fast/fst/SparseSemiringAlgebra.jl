# SPDX-License-Identifier: CECILL-2.1

"""
    AbstractSparseArray{Tv,Ti,N}

Supertype for sparse `N`-dimensional sparse arrays with elements of type `Tv`
and index type `Ti` (to store the coordinates of the non-zero elements).

!!! note
    Duplicate non-zero coordinates in sparse arrays will be merged "lazyily" in
    other operations.
"""
abstract type AbstractSparseArray{Tv,Ti,N} <: AbstractArray{Tv,N} end


"""
    AbstractSparseVector{Tv,Ti}

Supertype for sparse vectors with elements of type `Tv` and index type `Ti`.
"""
const AbstractSparseVector{Tv,Ti} = AbstractSparseArray{Tv,Ti,1} where {Tv,Ti}


"""
    AbstractSparseMatrix{Tv,Ti}

Supertype for sparse matrices with elements of type `Tv` and index type `Ti`.
"""
const AbstractSparseMatrix{Tv,Ti} = AbstractSparseArray{Tv,Ti,2} where {Tv,Ti}


"""
    nnz(x::AbstractSparseArray)

Return the number of non-zero element in `x`.
"""
nnz


"""
    nonzeros(x::AbstractSparseArray)

Return an iterable over the non-zero elements of `x`.
"""
nonzeros


"""
    findnz(x::AbstractSparseArray)

Return a tuple `(I, J, ..., V)` where `I, J, ...` are vectors of the
coordinates of the non-zero elements along the dimension `1, 2, ...` and `V`
is the vector associated values.
"""
findnz

