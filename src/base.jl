# SPDX-License-Identifier: CECILL-2.1


"""
    Base.copy(x::AbstractSparseArray)

Return a copy of `x`.
"""
Base.copy(x::SparseArray) = typeof(x)(x.dims, copy(x.nzcoo), copy(x.nzval))


function _cat!(new_nzcoo::Matrix{I}, xcoo::Matrix{I}, ycoo::Matrix{I}, xsize) where I
    xnnz = size(xcoo, 1)
    ynnz = size(ycoo, 1)
    N = size(new_nzcoo, 2)
    for n in 1:N
        @inbounds for nzi in 1:xnnz
            new_nzcoo[nzi,n] = xcoo[nzi,n]
        end
    end

    for n in 1:N
        offset = xsize[n]
        @inbounds for nzi in 1:ynnz
            new_nzcoo[xnnz+nzi,n] = offset + ycoo[nzi,n]
        end
    end

    new_nzcoo
end

"""
    Base.cat(x::AbstractSparseArray{T,N}, y::AbstractSparseArray{T,N}; dims) where T

Concatenate the `x` and `y` and return a sparse array. For `N = 2` this
build a block diagonal sparse matrix.
"""
Base.cat

function Base.cat(x::SparseArray{T,I,N}, y::SparseArray{T,I,N}; dims) where {T,I,N}
    # If the user has passed an integer convert it to a tuple
    dims = tuple(dims...)
    dims == tuple(1:N...) || throw(ArgumentError("`cat` with `dims` other than `tuple(1:N...)` is not supported for sparse arrays"))

    nzcoo = similar(x.nzcoo, nnz(x) + nnz(y), N)
    _cat!(nzcoo, x.nzcoo, y.nzcoo, size(x))
    nzval = vcat(x.nzval, y.nzval)
    newsize = ntuple(n -> size(x, n) + size(y, n), N)
    SparseArray{T,I,N}(newsize, nzcoo, nzval)
end

Base.vcat(x::AbstractSparseVector{T}, y::AbstractSparseVector{T}) where T = cat(x, y; dims = 1)


"""
    collapse(x)

Collapse duplicate non-zero coordinates in `x` by adding them and return a
array with no duplicate.
"""
collapse

function collapse(x::SparseArray{T,I,N}) where {T,I,N}
    coo_val = Dict{NTuple{N,I},T}()
    for nzi in 1:nnz(x)
        coo = ntuple(n -> x.nzcoo[nzi,n], N)
        val = x.nzval[nzi]
        coo_val[coo] = get(coo_val, coo, zero(T)) + val
    end

    new_nzcoo = Matrix{I}(undef, length(coo_val), N)
    new_nzval = Vector{T}(undef, length(coo_val))
    nzi = 1
    for (c, v) in coo_val
        for n in 1:N
            new_nzcoo[nzi,n] = c[n]
        end
        new_nzval[nzi] = v
    end

    SparseArray{T,I,N}(size(x), new_nzcoo, new_nzval)
end


function _sum!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}, dims::Dims) where I
    N = size(nzcoo, 2)
    M = length(dims)
    for n in 1:N
        v = n in dims
        @inbounds for nzi in 1:size(nzcoo, 1)
            new_nzcoo[nzi,n] = n in dims ? 1 : nzcoo[nzi,n]
        end
    end
    new_nzcoo
end

_sum!(new_nzcoo::AbstractVector{Dims{N}}, xcoo::AbstractVector{Dims{N}}, dims::Integer) where N =
    _sum(new_nzcoo, xcoo, (dims,))

function Base.sum(x::SparseArray{T,I,N}; dims = ntuple(identity, N)) where {T,I,N}
    dims == ntuple(identity, N) && return sum(x.nzval)
    newsize = ntuple(n -> n in dims ? 1 : size(x, n), N)
    new_nzcoo = similar(x.nzcoo)
    _sum!(new_nzcoo, x.nzcoo, dims)
    SparseArray{T,I,N}(newsize, new_nzcoo, x.nzval)
end


function _dropdims!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}, dims::Dims) where I
    nnz = size(nzcoo, 1)
    N = size(nzcoo, 2)
    M = length(dims)
    for i in 1:N-M

        # Skipped the dimensions to be dropped
        mi = i
        while mi in dims
            mi += 1
        end

        @inbounds @simd for nzi in 1:nnz
            new_nzcoo[nzi,i] = nzcoo[nzi,mi]
        end
    end
    new_nzcoo
end

_dropdims!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}, dims::Integer) where I = _dropdims!(new_nzcoo, nzcoo, (dims,))

function Base.dropdims(x::SparseArray{T,I,N}; dims = ntuple(identity, N)) where {T,I,N}
    newsize = ntuple(N-length(dims)) do i
        while i in dims
            i += 1
        end
        size(x, i)
    end
    M = length(dims)
    new_nzcoo = similar(x.nzcoo, nnz(x), N-M)
    _dropdims!(new_nzcoo, x.nzcoo, dims)
    SparseArray{T,I,N-M}(newsize, new_nzcoo, x.nzval)
end


function _permutedims!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}, p) where I
    N = length(p)
    for n in 1:N
        pn = p[n]
        for nzi in 1:size(nzcoo, 1)
            new_nzcoo[nzi,n] = nzcoo[nzi,pn]
        end
    end
    new_nzcoo
end

function Base.permutedims(x::SparseArray{T,I,N}, p) where {T,I,N}
    newsize = ntuple(n -> size(x, p[n]), N)
    new_nzcoo = similar(x.nzcoo)
    _permutedims!(new_nzcoo, x.nzcoo, p)
    SparseArray{T,I,N}(newsize, new_nzcoo, x.nzval)
end

