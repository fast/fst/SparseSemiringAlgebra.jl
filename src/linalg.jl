# SPDX-License-Identifier: CECILL-2.1

# Define the transpose of a semiring scalar to be itself.
@inline Base.transpose(x::Semiring) = x
@inline LinearAlgebra.dot(x::T, y::T) where T<:Semiring = x * y

function _transpose_vec!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}) where I
    @inbounds @simd for nzi in 1:size(nzcoo, 1)
        new_nzcoo[nzi,1] = 1
        new_nzcoo[nzi,2] = nzcoo[nzi,1]
    end
    new_nzcoo
end

function _transpose_mat!(new_nzcoo::Matrix{I}, nzcoo::Matrix{I}) where I
    @inbounds @simd for nzi in 1:size(nzcoo, 1)
        new_nzcoo[nzi,1] = nzcoo[nzi,2]
        new_nzcoo[nzi,2] = nzcoo[nzi,1]
    end
    new_nzcoo
end

function Base.copy(xᵀ::Transpose{T,<:SparseVector{T,I}}) where {T,I}
    x = parent(xᵀ)
    new_nzcoo = similar(x.nzcoo, 2)
    _transpose_vec!(new_nzcoo, x.nzcoo)
    SparseMatrix{T,I}((1, size(x, 1)), new_nzcoo, x.nzval)
end

function Base.copy(xᵀ::Transpose{T,<:SparseMatrix{T,I}}) where {T,I}
    x = parent(xᵀ)
    new_nzcoo = similar(x.nzcoo)
    _transpose_mat!(new_nzcoo, x.nzcoo)
    SparseMatrix{T,I}((size(x, 2), size(x, 1)), new_nzcoo, x.nzval)
end


#==============================================================================
Scalar multiplication
==============================================================================#

function _scalarmul!(out, binop, a::T, values::Vector{T}) where T
    @inbounds @simd for i in 1:length(values)
        out[i] = binop(a, values[i])
    end
    out
end

function Base.:*(a::T, x::SparseArray{T,I,N}) where {T,I,N}
    if iszero(a)
        spzeros(T, I, size(x)...)
    else
        new_nzval= similar(x.nzval)
        _scalarmul!(new_nzval, (u, v) -> u*v, a, x.nzval)
        SparseArray{T,I,N}(size(x), x.nzcoo, new_nzval)
    end
end

function Base.:*(x::SparseArray{T,I,N}, a::T) where {T,I,N}
    if iszero(a)
        spzeros(T, size(x)...)
    else
        new_nzval= similar(x.nzval)
        _scalarmul!(new_nzval, (u, v) -> v*u, a, x.nzval)
        SparseArray{T,I,N}(size(x), x.nzcoo, new_nzval)
    end
end


#==============================================================================
Inner product
==============================================================================#

function _dot_dvspv(op, x::Vector{T}, ycoo::Matrix{I}, yval::Vector{T}) where {T,I}
    result = zero(T)
    # @simd hurts the perf. because of the random memory access.
    @inbounds for nzi in 1:length(yval)
        i = ycoo[nzi,1]
        result = result + op(x[i], yval[nzi])
    end
    result
end

function _dot_spvspv(buffer::Vector{T}, xcoo::Matrix{I}, xval::Vector{T}, ycoo::Matrix{I}, yval::Vector{T}, len) where {T,I}
    fill!(buffer, zero(T))

    @inbounds for nzi in 1:length(xval)
        i, = xcoo[nzi,1]
        buffer[i] = xval[nzi]
    end

    result = zero(T)
    @inbounds for nzi in 1:length(yval)
        i, = ycoo[nzi,1]
        result = result + buffer[i] * yval[nzi]
    end
    result
end

function LinearAlgebra.dot(x::Vector{T}, y::SparseVector{T}) where T
    @assert size(x) == size(y)
    size(x) ≠ size(y) && throw(DimensionMistmatch("`x` and `y` should have the same size."))
    _dot_dvspv((u, v) -> u * v, x, y.nzcoo, y.nzval)
end

function LinearAlgebra.dot(x::AbstractSparseVector{T}, y::AbstractVector{T}) where T
    @assert size(x) == size(y)
    _dot_dvspv((u, v) -> v * u, y, x.nzcoo, x.nzval)
end

function LinearAlgebra.dot(x::AbstractSparseVector{T}, y::AbstractSparseVector{T}) where T
    @assert size(x) == size(y)
    buffer = similar(x.nzval, size(x, 1))
    _dot_spvspv(buffer, x.nzcoo, x.nzval, y.nzcoo, y.nzval)
end


#==============================================================================
Vector outer product
==============================================================================#

function _outer_spvspv!(zcoo::Matrix{I}, zval::Vector{T}, xcoo::Matrix{I}, xval::Vector{T}, ycoo::Matrix{I}, yval::Vector{T}) where {T,I}
    xnnz = length(xval)
    ynnz = length(yval)
    znzi = 1
    for xnzi in 1:xnnz
        @inbounds @simd for ynzi in 1:ynnz
            zcoo[znzi,1] = xcoo[xnzi,1]
            zcoo[znzi,2] = ycoo[ynzi,1]
            zval[znzi] = xval[xnzi] * yval[ynzi]
            znzi += 1
        end
    end
    zcoo, zval
end

function Base.:*(x::SparseVector{T,I}, yᵀ::Transpose{T,<:SparseVector{T,I}}) where {T,I}
    y = parent(yᵀ)
    new_nzcoo = similar(x.nzcoo, nnz(x) * nnz(y), 2)
    new_nzval = similar(x.nzval, nnz(x) * nnz(y))
    _outer_spvspv!(new_nzcoo, new_nzval, x.nzcoo, x.nzval, y.nzcoo, y.nzval)
    SparseMatrix{T,I}((size(x, 1), size(y, 1)), new_nzcoo, new_nzval)
end


#==============================================================================
Elementwise add
==============================================================================#

function _add_dtspt!(out::Vector{T}, x::Vector{T}, ycoo::Matrix{I}, yval::Vector{T}) where {T,I}
    copyto!(out, x)
    @inbounds for nzi in 1:length(yval)
        i = ycoo[nzi,1]
        out[i] = out[i] + yval[nzi]
    end
    out
end

function _add_dtspt!(out::Matrix{T}, x::Matrix{T}, ycoo::Matrix{I}, yval::Vector{T}) where {T,I}
    copyto!(out, x)
    @inbounds for nzi in 1:length(yval)
        i = ycoo[nzi,1]
        j = ycoo[nzi,2]
        out[i,j] = out[i,j] + yval[nzi]
    end
    out
end

## dense + sparse

function Base.:+(x::Array{T,N}, y::SparseArray{T,I,N}) where {T,I,N}
    size(x) == size(y) || throw(DimensionMismatch("`x` and `y` should have the same size"))
    out = similar(x)
    _add_dtspt!(out, x, y.nzcoo, y.nzval)
    out
end

Base.:+(x::SparseArray{T,I,N}, y::Array{T,N}) where {T,I,N} = y + x


## sparse + sparse (lazy operations)

function Base.:+(x::SparseArray{T,I,N}, y::SparseArray{T,I,N}) where {T,I,N}
    size(x) == size(y) || throw(DimensionMismatch("`x` and `y` should have the same size"))
    nzcoo = vcat(x.nzcoo, y.nzcoo)
    nzval = vcat(x.nzval, y.nzval)
    SparseArray{T,I,N}(size(x), nzcoo, nzval)
end


#==============================================================================
Matrix-vector multiplication
==============================================================================#

# sparse matrix - dense vector multiplication
function _mvmul_spmdv!(out::Vector{T},
                       op,
                       Aptr::Vector{I},
                       Acoo::Matrix{I},
                       Aval::Vector{T},
                       x::Vector{T},
                       sorted_dim) where {T,I}
    for d in 1:length(out)
        v = zero(T)
        @inbounds for nzi in Aptr[d]:Aptr[d+1]-1
            i = Acoo[nzi,sorted_dim]
            v = v + op(Aval[nzi], x[i])
        end
        out[d] = v
    end
end


function Base.:*(A::AbstractSparseMatrix{T}, x::Vector{T}) where T
    @assert size(A, 2) == size(x, 1)
    out = similar(x, size(A, 1))
    sA = sort(A, 1)
    _mvmul_spmdv!(out, (u, v) -> u * v, sA.ptr, sA.nzcoo, sA.nzval, x, 2)
    out
end

function Base.:*(xᵀ::Transpose{T,<:AbstractVector{T}}, A::AbstractSparseMatrix{T}) where T
    x = parent(xᵀ)
    @assert size(A, 1) == size(x, 1)
    out = similar(x, size(A, 2))
    sA = sort(A, 2)
    _mvmul_spmdv!(out, (u, v) -> v * u, sA.ptr, sA.nzcoo, sA.nzval, x, 1)
    transpose(out)
end


#==============================================================================
Kronecker product
==============================================================================#


function _kron!(zcoo::Matrix{I},
                zval::Vector{T},
                xcoo::Matrix{I},
                xval::Vector{T},
                ycoo::Matrix{I},
                yval::Vector{T},
                ysize) where {T,I}
    N = size(zcoo, 2)
    xnnz = length(xval)
    ynnz = length(yval)


    for n in 1:N
        znzi = 1
        for xnzi in 1:xnnz
            for ynzi in 1:ynnz
                zcoo[znzi,n] = (xcoo[xnzi,n] - 1) * ysize[n] + ycoo[ynzi,n]
                znzi += 1
            end
        end
    end

    znzi = 1
    for xnzi in 1:xnnz
        for ynzi in 1:ynnz
            zval[znzi] = xval[xnzi] * yval[ynzi]
            znzi += 1
        end
    end

    zcoo, zval
end


function Base.kron(x::SparseArray{T,I,N}, y::SparseArray{T,I,N}) where {T,I,N}
    nzcoo = similar(x.nzcoo, nnz(x) * nnz(y), size(x, 2))
    nzval = similar(x.nzval, nnz(x) * nnz(y))
    _kron!(nzcoo, nzval, x.nzcoo, x.nzval, y.nzcoo, y.nzval, size(y))
    newsize = ntuple(n -> size(x, n) * size(y, n), N)
    SparseArray{T,I,N}(newsize, nzcoo, nzval)
end

#function tensorproduct(x::AbstractSparseArray{T,M}, y::AbstractSparseArray{T,N}) where {T,M,N}
#    coordinates = similar(x.coordinates, Dims{M+N}, nnz(x) * nnz(y))
#    nzi = 1
#    for xcoo in x.coordinates, ycoo in y.coordinates
#        coordinates[nzi] = (xcoo..., ycoo...)
#        nzi += 1
#    end
#
#    values = similar(x.values, nnz(x) * nnz(y))
#    nzi = 1
#    for xval in x.values, yval in y.values
#        values[nzi] = xval * yval
#        nzi += 1
#    end
#
#    newsize = (size(x)..., size(y)...)
#    sparse(coordinates, values, newsize...)
#end

#==============================================================================
Nullspace

We assume that the semiring is zerosumfree. Under this condition, finding the
nullspace is equivalent to identify the column of the matirx `A` that have only
0̄ elements.
==============================================================================#

function LinearAlgebra.nullspace(A::AbstractMatrix{T}; keepdims = false) where T<:Semiring
    zero_columns = Int[]
    for col in 1:size(A, 2)
        has_nonzero_value = false
        for row in 1:size(A, 1)
            if ! iszero(A[row,col])
                has_nonzero_value = true
            end
        end

        if ! has_nonzero_value
            push!(zero_columns, col)
        end
    end

    nbases = keepdims ? max(size(A, 2), length(zero_columns)) : length(zero_columns)
    bases = fill!(similar(A, size(A, 2), nbases), zero(T))
    for b in 1:length(zero_columns)
        bases[zero_columns[b],b] = one(T)
    end

    bases
end

function LinearAlgebra.nullspace(Aᵀ::Transpose{T,<:AbstractMatrix{T}}; keepdims = false) where T<:Semiring
    A = parent(Aᵀ)

    zero_rows = Int[]
    for row in 1:size(A, 1)
        has_nonzero_value = false
        for col in 1:size(A, 2)
            if ! iszero(A[row,col])
                has_nonzero_value = true
            end
        end

        if ! has_nonzero_value
            push!(zero_rows, row)
        end
    end

    nbases = keepdims ? max(size(A, 1), length(zero_rows)) : length(zero_rows)
    bases = fill!(similar(A, size(A, 1), nbases), zero(T))
    for b in 1:length(zero_rows)
        bases[zero_rows[b],b] = one(T)
    end

    bases
end

function LinearAlgebra.nullspace(A::SparseMatrix{T,I}; keepdims = false) where {T<:Semiring,I}

    d = ones(Bool, size(A, 2))
    for nzi in 1:nnz(A)
        (row, col) = A.nzcoo[nzi,:]
        d[col] = iszero(A.nzval[nzi])
    end

    zero_columns = findall(identity, d)
    nbases = keepdims ? max(size(A, 2), length(zero_columns)) : length(zero_columns)

    nzcoo = similar(A.nzcoo, length(zero_columns), nbases)
    for (n, ci) in enumerate(zero_columns)
        nzcoo[n,1] = (1:size(A, 2))[ci]
        nzcoo[n,2] = n
    end
    nzval = ones(T, length(zero_columns))
    SparseMatrix{T,I}((size(A, 2), nbases), nzcoo, nzval)
end

