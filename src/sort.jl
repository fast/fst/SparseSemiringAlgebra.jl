# SPDX-License-Identifier: CECILL-2.1

"""
    SortedSparseArray{S,T,I,N} <: AbstractSparseArray{T,I,N}

Sparse array with non-zero elements sorted along the dimension `S` of their
coordinates.
"""
struct SortedSparseArray{S,T,I,N} <: AbstractSparseArray{T,I,N}
    dims::Dims{N}
    ptr::Vector{I}
    nzcoo::Matrix{I}
    nzval::Vector{T}

    function SortedSparseArray{S,T,I,N}(dims::Dims{N}, ptr::Vector{I}, nzcoo::Matrix{I}, nzval::Vector{T}) where {S,T,I,N}
        Base.size(nzcoo, 1) == length(nzval) || throw(ArgumentError("`size(nzcoo, 1)` should be equal to `length(nzval)`"))
        length(ptr) == dims[S]+1 || throw(ArgumentError("`length(ptr)` should be equal to `dims[S]+1`"))

        new{S,T,I,N}(dims, ptr, nzcoo, nzval)
    end
end

const SortedSparseMatrix{S,T,I} = SortedSparseArray{S,T,I,2}

Base.size(x::SortedSparseArray) = x.dims

function Base.getindex(x::SortedSparseArray{S,T,I,N}, ind::Vararg{Integer,N}) where {S,T,I,N}
    result = zero(T)
    for s in 1:size(x, S)
        for nzi in x.ptr[s]:x.ptr[s+1]-1
            coo = ntuple(n -> x.nzcoo[nzi,n], N)
            if coo == ind
                result += x.nzval[nzi]
            end
        end
    end
    result
end

nnz(x::SortedSparseArray) = length(x.nzval)


"""
    Base.sort(x::AbstractSparseArray, dim = 1)

Return a sparse array with the non-zero elements of `x` sorted along the `dim`
dimension of their coordinates.
"""
Base.sort

function _sort(nzcoo::Matrix{I}, nzval::Vector{T}, dim, dimlength) where {T,I}
    size(nzcoo, 1) == length(nzval) || throw(ArgumentError("`nzcoo` and `nzval` should have the same length"))

    N = size(nzcoo, 2)
    nnz = length(nzval)

    # Count the number of non-zero entries for each slice of A along the
    # `dim` dimension.
    ptr = zeros(I, dimlength+1)
    ptr[1] = I(1)
    for nzi in 1:nnz
        ptr[nzcoo[nzi,dim]+1] += one(I)
    end

    # Cumulative sum to compute the number of non-zero elements. Set in
    # `ptr[i]` the number of non-zero elements up to the `i`th slice.
    nnz = 0
    @inbounds for i in 2:dimlength+1
        tmp = ptr[i]
        ptr[i] = nnz + 1
        nnz += tmp
    end

    # Write the coordinates and the values.
    new_nzcoo = Matrix{I}(undef, nnz, N)
    new_nzval = Vector{T}(undef, nnz)
    for nzi in 1:nnz
        dest = ptr[nzcoo[nzi,dim]+1]
        new_nzval[dest] = nzval[nzi]
        for n in 1:N
            new_nzcoo[dest,n] = nzcoo[nzi,n]
        end
        ptr[nzcoo[nzi,dim]+1] += 1
    end

    ptr, new_nzcoo, new_nzval
end

function Base.sort(x::SparseArray{T,I,N}, dim = 1) where {T,I,N}
    ptr, nzcoo, nzval = _sort(x.nzcoo, x.nzval, dim, size(x, dim))
    SortedSparseArray{dim,T,I,N}(size(x), ptr, nzcoo, nzval)
end

function Base.sort(x::SortedSparseArray{S,T,I,N}, dim = 1) where {S,T,I,N}
    # `x` is already sorted return it unchanged.
    dim == S && return x
    nzcoo, nzval, ptr = _sort(x.nzcoo, x.nzval, dim, size(x, dim))
    SortedSparseArray{dim,T,I,N}(size(x), ptr, nzcoo, nzval)
end

