# SPDX-License-Identifier: CECILL-2.1

#=============================================================================#
# Generic sparse array
#=============================================================================#

"""
    SparseArray{Tv,Ti,N} <: AbstractSparseArray{Tv,N}

`N`-dimensional sparse array of `Tv` and index type `Ti`.

# Specific Aliases
    const SparseMatrix{Tv,Ti} = SparseArray{Tv,Ti,2} where {Tv,Ti}
    const SparseVector{Tv,Ti} = SparseArray{Tv,Ti,1} where {Tv,Ti}

# Examples
```julia-repl
```
"""
struct SparseArray{Tv,Ti,N} <: AbstractSparseArray{Tv,Ti,N}
    dims::Dims{N}
    nzcoo::Vector{Vector{Ti}}
    nzval::Vector{Tv}

    function SparseArray{Tv,Ti,N}(dims, nzcoo, nzval) where {Tv,Ti,N}
        @assert N == length(nzcoo) "inconsistent length for `nzcoo`"
        @assert all(length.(nzcoo) .== Ref(length(nzval)))
        new{Tv,Ti,N}(dims, nzcoo, nzval)
    end

    function SparseArray{Tv,Ti,N}(::UndefInitializer, dims::Vararg{Integer,N}) where {Tv,Ti,N}
        nzcoo = [Vector{Ti}(undef, 0) for _ in 1:N]
        SparseArray{Tv,Ti,N}(dims, nzcoo, Tv[])
    end
end

const SparseMatrix{Tv,Ti} = SparseArray{Tv,Ti,2} where {Tv,Ti}
const SparseVector{Tv,Ti} = SparseArray{Tv,Ti,1} where {Tv,Ti}


Base.similar(x::SparseArray{Tv,Ti,N}, ::Type{Tnew}, dims::Dims{Nnew}) where {Tv,Ti,N,Tnew,Nnew} = spzeros(Tnew, Ti, dims...)

Base.size(x::SparseArray) = x.dims

function Base.getindex(x::SparseArray{Tv,Ti,N}, ind::Vararg{Integer,N}) where {Tv,Ti,N}
    result = zero(Tv)
    for nzi in 1:nnz(x)
        coo = ntuple(n -> x.nzcoo[n][nzi], N)
        if coo == ind
            result = result + x.nzval[nzi]
        end
    end
    result
end


function Base.setindex!(x::SparseArray{Tv,Ti,N}, v, ind::Vararg{Integer,N}) where {Tv,Ti,N}
    result = zero(Tv)
    inds = findall(nzi -> ntuple(n -> x.nzcoo[n][nzi], N) == ind, 1:nnz(x))
    for n in 1:N
        deleteat!(x.nzcoo[n], inds)
        push!(x.nzcoo[n], ind[n])
    end
    deleteat!(x.nzval, inds)
    push!(x.nzval, v)
    v
end

### SparseArray interface

coordinates(x::SparseArray) = x.nzcoo
nnz(x::SparseArray) = length(x.nzval)
nonzeros(x::SparseArray) = x.nzval

### Constructors

"""
    sparse(nzcoo, nzval[, dims...])

Create a sparse array of size `dims` from a list of non-zero values `nzval` and
their associated coordinates `nzcoo`.
"""
sparse

function sparse(nzcoo::Vector{Vector{Ti}}, nzval::AbstractVector{Tv}, dim1::Integer, extra_dims::Vararg{Integer,N}) where {Tv,Ti,N}
    dims = (dim1, extra_dims...)
    SparseArray{Tv,Ti,N+1}(dims, nzcoo, nzval)
end

function sparse(nzcoo::Vector{Vector{Ti}}, value::Tv, dims::Vararg{Integer,N}) where {Tv,Ti,N}
    nnz = isempty(nzcoo) ? 0 : length(first(nzcoo))
    nzval = fill!(Vector{Tv}(undef, nnz), value)
    sparse(nzcoo, nzval, dims...)
end

function sparse(nzcoo::Vector{Vector{Ti}}, nzval::AbstractVector{Tv}) where {Tv,Ti}
    sparse(nzcoo, nzval, maximum.(nzcoo, init = 0)...)
end


"""
    spzeros([Tv = Float64, Ti = Int, V = Vector{Tv}], dims...)

Return a sparse array of type `SparseArray{Tv,Ti,length(dims),V}` with no
non-zero element.
"""
spzeros

spzeros(Tv::Type, Ti::Type, dims::Vararg{Integer,N}) where N = SparseArray{Tv,Ti,N}(undef, dims...)
spzeros(Tv::Type, dims::Vararg{Integer,N}) where N = spzeros(Tv, Int, dims...)
spzeros(dims::Vararg{Integer,N}) where N = spzeros(Float64, Int, dims...)


#=============================================================================#
# Vector of sparse array.
#=============================================================================#

"""
    VecSparseArray{Tv,Ti,N} <: AbstractVector{SparseArray{Tv,Ti,N}}

A vector of [`SparseArray`](@ref) with identical size.
"""
struct VecSparseArray{Tv,Ti,N} <: AbstractVector{SparseArray{Tv,Ti,N}}
    arraydims::Dims{N}
    ptr::Vector{Ti}
    nzcoo::Vector{Vector{Ti}}
    nzval::Vector{Tv}

    function VecSparseArray{Tv,Ti,N}(dims::Dims{N}, ptr::Vector{Ti}, nzcoo::Vector{Vector{Ti}}, nzval::Vector{Tv}) where {Tv,Ti,N}
        @assert N == length(nzcoo) "inconsistent dimension for sparse array"
        @assert all(length.(nzcoo) .== length(nzval))
        new{Tv,Ti,N}(dims, ptr, nzcoo, nzval)
    end
end

Base.size(x::VecSparseArray) = (length(x.ptr) - 1,)

function Base.getindex(x::VecSparseArray{Tv,Ti,N}, i::Integer) where {Tv,Ti,N}
    range = x.ptr[i]:x.ptr[i+1]-1
    nzcoo = [x.nzcoo[n][range] for n in 1:N]
    nzval = x.nzval[range]
    SparseArray{Tv,Ti,N}(x.arraydims, nzcoo, nzval)
end

function Base.getindex(x::VecSparseArray{Tv,Ti,N}, r::AbstractUnitRange) where {Tv,Ti,N}
    start, stop = first(r), last(r)
    range = x.ptr[start]:x.ptr[stop+1]-1
    ptr = x.ptr[start:stop+1]
    nzcoo = [x.nzcoo[n][range] for n in 1:N]
    nzval = x.nzval[range]
    VecSparseArray{Tv,Ti,N}(x.arraydims, ptr, nzcoo, nzval)
end

### SparseArray interface

coordinates(x::VecSparseArray) = x.nzcoo
nnz(x::VecSparseArray) = length(x.nzval)
nonzeros(x::VecSparseArray) = x.nzval

### Constructors

function vsparse(sparray::SA, others::SA...) where SA<:SparseArray
    @assert all(size.(others) .== Ref(size(sparray)))

    sparrays = [sparray, others...]
    Tv = eltype(sparray)
    Ti = eltype(eltype(coordinates(sparray)))
    N = ndims(sparray)

    ptr = zeros(Ti, length(sparrays)+1)
    ptr[1] = 1
    for (i, x) in enumerate(sparrays)
        ptr[i+1] = ptr[i] + nnz(x)
    end

    nzcoo = [vcat([coordinates(m)[n] for m in sparrays]...) for n in 1:N]
    nzval = vcat([nonzeros(m) for m in sparrays]...)

    VecSparseArray{Tv,Ti,N}(size(sparray), ptr, nzcoo, nzval)
end


#=============================================================================#
# Square sparse matrix.
#=============================================================================#

#struct Square{Tv,Ti,D,SpM<:AbstractSparseMatrix{Tv,Ti}} <: AbstractSparseMatrix{Tv,Ti}
#    spmat::SpM
#
#    function Square(x::AbstractSparseMatrix{Tv,Ti}) where {Tv,Ti}
#        @assert size(x, 1) == size(x, 2)
#        new{Tv,Ti,size(x, 1),typeof(x)}(x)
#    end
#
#    Square{Tv,Ti,D}(dims, nzcoo, nzval) where {Tv,Ti,D} =
#        Square(SparseMatrix{Tv,Ti}(dims, nzcoo, nzval))
#end
#
#### Array interface
#
#Base.parent(x::Square) = x.spmat
#Base.size(x::Square) = size(x.spmat)
#Base.getindex(x::Square, ii...) = getindex(x.spmat, ii...)
#
#### SparseArray interface
#
#coordinates(x::Square) = coordinates(x.spmat)
#nnz(x::Square) = nnz(x.spmat)
#nonzeros(x::Square) = nonzeros(x.spmat)
#
#
#Base.zero(M::Type{<:Square}) = M(undef)


"""
    NestedSparseArray{Tv,Ti,N} <: AbstractSparseArray{Tv,N}

`N`-dimensional sparse array of `Tv` and index type `Ti`.

# Specific Aliases
    const SparseMatrix{Tv,Ti} = SparseArray{Tv,Ti,2} where {Tv,Ti}
    const SparseVector{Tv,Ti} = SparseArray{Tv,Ti,1} where {Tv,Ti}

# Examples
```julia-repl
```
"""
struct NestedSparseArray{Tv,Ti,N,M} <: AbstractSparseArray{Tv,Ti,N}
    dims::Dims{N}
    nzcoo::Vector{Vector{Ti}}
    nzval::VecSparseArray{Tv,Ti,M}

    function SparseArray{Tv,Ti,N}(dims, nzcoo, nzval) where {Tv,Ti,N}
        @assert N == length(nzcoo) "inconsistent length for `nzcoo`"
        @assert all(length.(nzcoo) .== Ref(length(nzval)))
        new{Tv,Ti,N}(dims, nzcoo, nzval)
    end

    function SparseArray{Tv,Ti,N}(::UndefInitializer, dims::Vararg{Integer,N}) where {Tv,Ti,N}
        nzcoo = [Vector{Ti}(undef, 0) for _ in 1:N]
        SparseArray{Tv,Ti,N}(dims, nzcoo, Tv[])
    end
end


#=============================================================================#
# Pretty printing.
#=============================================================================#

Base.show(io::IO, x::SparseArray) = print(io, "sparse(", coordinates(x), ", ", nonzeros(x), ", ", join(size(x), ", "), ")")
#Base.show(io::IO, x::Square) = print(io, "Square(", x.spmat, ")")
#Base.show(io::IO, x::VecSparseArray) = print(io, "vsparse(", join(x, ","), ")")


function Base.array_summary(io::IO, x::AbstractSparseArray, dims::Tuple{Vararg{Base.OneTo}})
    xnnz = nnz(x)
    print(io, join(size(x), "x"), " ", typeof(x), " with ", xnnz, " stored ",
          xnnz == 1 ? "entry" : "entries")
    nothing
end

function Base.array_summary(io::IO, x::AbstractSparseArray{<:Any,<:Any,1}, dims::Tuple{Vararg{Base.OneTo}})
    xnnz = nnz(x)
    print(io, size(x, 1), "-element", " ", typeof(x), " with ", xnnz, " stored ",
          xnnz == 1 ? "entry" : "entries")
    nothing
end

#function Base.array_summary(io::IO, x::VecSparseArray, dims::Tuple{Vararg{Base.OneTo}})
#    xnnz = nnz(x)
#    print(io, size(x, 1), "-element", " ", typeof(x), " with a total of ", xnnz, " stored ",
#          xnnz == 1 ? "entry" : "entries")
#    nothing
#end

function Base.replace_in_print_matrix(x::AbstractSparseArray{<:Any,<:Any,2}, i::Integer, j::Integer, s::AbstractString)
    found = false
    nzcoo = coordinates(x)
    for nzi in 1:nnz(x)
        coo = ntuple(n -> nzcoo[n][nzi], 2)
        if coo == (i, j)
            found = true
            break
        end
    end
    found ? s : Base.replace_with_centered_mark(s)
end

function Base.replace_in_print_matrix(x::AbstractSparseArray{<:Any,<:Any,1}, i::Integer, j::Integer, s::AbstractString)
    found = false
    nzcoo = coordinates(x)
    for nzi in 1:nnz(x)
        coo = ntuple(n -> nzcoo[n][nzi], 1)
        if coo == (i,)
            found = true
            break
        end
    end
    found ? s : Base.replace_with_centered_mark(s)
end

