# SPDX-License-Identifier: CECILL-2.1

"""
    SparseVector{Tv,Ti} <: AbstractSparseArray{Tv,Ti,1}

Sparse vector of element type `Tv` and index type `Ti`.
"""
struct SparseVector{Tv,Ti} <: AbstractSparseArray{Tv,Ti,1}
    length::Int
    nzind::Vector{Ti}
    nzval::Vector{Tv}

    function SparseVector{Tv,Ti}(length, nzind, nzval) where {Tv,Ti}
        @assert all(Base.length(nzind) == Base.length(nzval))

        p = sortperm(nzind)
        new{Tv,Ti}(length, nzind[p], nzval[p])
    end

    function SparseVector{Tv,Ti}(::UndefInitializer, length::Integer, nnz = 0) where {Tv,Ti}
        new{Tv,Ti}(length, Vector{Ti}(undef, nnz), Vector{Tv}(undef, nnz))
    end
end


Base.size(x::SparseVector) = (x.length,)

function Base.getindex(x::SparseVector{Tv}, i::Integer) where Tv
    result = zero(Tv)
    nzind, nzval = findnz(x)
    for nzi in searchsorted(nzind, i)
        result += nzval[nzi]
    end
    result
end

function Base.setindex!(x::SparseVector{Tv,Ti}, v, i::Integer) where {Tv,Ti}
    result = zero(Tv)
    nzind, nzval = findnz(x)
    nzr = searchsorted(nzind, i)
    deleteat!(nzind, nzr)
    deleteat!(nzval, nzr)

    idx = first(nzr)
    insert!(nzind, idx, i)
    insert!(nzval, idx, v)

    v
end

### AbstractSparseArray interface

findnz(x::SparseVector) = (x.nzind, x.nzval)
nnz(x::SparseVector) = length(x.nzval)
nonzeros(x::SparseVector) = x.nzval
spzeros(Tv::Type, Ti::Type, length::Integer) = SparseVector{Tv,Ti}(undef, length)


"""
    sparsevec(nzind, nzval[, length])

Create a sparse vector from a vector of indices `nzind` and a vector of
non-zero values `nzval`.
"""
sparsevec(nzind::Vector{Ti}, nzval::Vector{Tv}, length = maximum(nzind)) where {Tv,Ti} =
    SparseVector{Tv,Ti}(length, nzind, nzval)

### Base operations

function _vcat!(y::SparseVector{Tv,Ti}, xs::SparseVector{Tv,Ti}...) where {Tv,Ti}
    total_nnz = sum(nnz, xs)
    @assert nnz(y) == total_nnz

    new_nzind, new_nzval = findnz(y)

    acc_len = 0
    acc_nnz = 0
    for x in xs
        nzind, nzval = findnz(x)
        for nzi in 1:nnz(x)
            new_nzind[acc_nnz + nzi] = acc_len + nzind[nzi]
            new_nzval[acc_nnz + nzi] = nzval[nzi]
        end
        acc_len += length(x)
        acc_nnz += nnz(x)
    end

    y
end

function Base.vcat(xs::SparseVector{Tv,Ti}...) where {Tv,Ti}
    total_len = sum(length, xs)
    total_nnz = sum(nnz, xs)
    y = SparseVector{Tv,Ti}(undef, total_len, total_nnz)
    _vcat!(y, xs...)
end

