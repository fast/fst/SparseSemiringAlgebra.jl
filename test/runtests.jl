
using LinearAlgebra
using Semirings
using SparseSemiringAlgebra
using Test

const SSA = SparseSemiringAlgebra

Base.isapprox(x::AbstractVector{T}, y::AbstractVector{T}) where T<:Semiring =
    length(x) == length(y) && mapreduce((a, b) -> isapprox(a, b), &, x, y)


@testset verbose=true "Sparse arrays" begin
    Tv = ProbSemiring{Float32}
    Ti = Int32

    @testset verbose=true "sparse vector" begin
        x = sparsevec(Ti[2, 4], Tv[10, 20], 5)
        @test x isa SSA.SparseVector{Tv,Ti}
        @test x == Tv[0, 10, 0, 20, 0]
        @test size(x) == (5,)
        @test nnz(x) == 2
        nzind, nzval = findnz(x)
        @test nzind == [2, 4]
        @test nzval ≈ Tv[10, 20]

        x = sparsevec(Ti[2, 2, 4], Tv[10, 5, 20], 5)
        x[2] = Tv(3)
        nzind, nzval = findnz(x)
        @test nzind == [2, 4]
        @test nzval ≈ Tv[3, 20]

        x = sparsevec(Ti[2, 4], Tv[10, 20])
        @test size(x) == (4,)

        x = spzeros(Tv, Ti, 3)
        @test size(x) == (3,)
        @test nnz(x) == 0

        x = sparsevec(Ti[2, 4], Tv[10, 20], 5)
        y = sparsevec(Ti[2, 3], Tv[30, 40], 3)
        z = sparsevec(Ti[5], Tv[50], 6)
        u = vcat(x, y, z)
        @test nnz(u) == nnz(x) + nnz(y) + nnz(z)
        @test size(u, 1) == size(x, 1) + size(y, 1) + size(z, 1)
        nzind, nzval = findnz(u)
        @test nzind == Ti[2, 4, 7, 8, 13]
        @test nzval == Tv[10, 20, 30, 40, 50]
    end

    #@testset verbose=true "sparse matrix" begin
    #    nzcoo = [
    #        [1, 1, 2, 2, 3, 3, 3, 4],
    #        [1, 2, 2, 4, 3, 4, 5, 6]
    #    ]
    #    nzval = Tv[10, 20, 30, 40, 50, 60, 70, 80]
    #    y = sparse(nzcoo, nzval, 4, 6)
    #    @test size(y) == (4, 6)
    #    @test nnz(y) == 8
    #    @test coordinates(y) == nzcoo
    #    @test nonzeros(y) ≈ Tv[10, 20, 30, 40, 50, 60, 70, 80]

    #    x = sparse([Ti[1, 2, 1], Ti[3, 2, 3]], Tv[10, 20, 30], 3, 3)
    #    x[1,3] = 30
    #    @test x == Tv[ 0  0 30;
    #                   0 20  0;
    #                   0  0  0]

    #    x = sparse([Ti[2, 3], Ti[1, 4]], Tv[10, 20], 5, 5)
    #    @test similar(x, ProbSemiring{Float64}) isa SparseArray{ProbSemiring{Float64},Ti,2}
    #    @test similar(x, ProbSemiring{Float64}, 1, 2, 3) isa SparseArray{ProbSemiring{Float64},Ti,3}
    #    @test size(similar(x, ProbSemiring{Float64}, 1, 2, 3)) == (1, 2, 3)
    #    @test nnz(similar(x, ProbSemiring{Float64})) == 0

    #end

    #@testset verbose=true "square sparse matrix" begin
    #    A = sparse(Ti[1 2; 1 3; 2 1; 3 3], Tv[10, 20, 30, 40], 3, 3)
    #    A = sparse(NTuple{2,Ti}[(1, 2), (1, 3), (2, 1), (3, 3)], Tv[
    #end


    #@testset "sort" begin
    #    x = sparsevec([2, 1, 3, 2], T[2, 1, 3, 2])
    #    sx = sort(x, 1)
    #    @test size(x) == size(sx)
    #    @test nnz(x) == nnz(sx)
    #    @test all(sx.nzcoo .== [1; 2; 2; 3])
    #    @test all(val.(sx.nzval) .≈ [1, 2, 2, 3])
    #    @test all(val.(sx) .≈ [1, 4, 3])

    #    x = sparse([(1, 3), (2, 2), (3, 1)], T[10, 20, 30])
    #    sx = sort(x, 2)
    #    @test all(sx.nzcoo .== [3 1; 2 2; 1 3])
    #    @test size(x) == size(sx)
    #    @test nnz(x) == nnz(sx)
    #    @show sx.nzcoo
    #    @show sx.nzval
    #    @show val.(sx)
    #    @test all(val.(sx) .≈ [ 0  0 10;
    #                            0 20  0;
    #                           30  0  0])
    #end
end


#@testset verbose=true "base" begin
#    T = ProbSemiring{Float32}
#    @testset "cat" begin
#        x = sparsevec([1, 3], T[1, 2], 4)
#        y = sparsevec([2], T[10], 5)
#        z = cat(x, y; dims = 1)
#        @test z isa AbstractSparseArray
#        @test size(z, 1) == size(x, 1) + size(y, 1)
#        @test all(val.(z) .≈ [1, 0, 2, 0, 0, 10, 0, 0, 0])
#        z = vcat(x, zero(T) * y)
#        @test all(val.(z) .≈ [1, 0, 2, 0, 0, 0, 0, 0, 0])
#        @test length(z.nzcoo) == nnz(x)
#        z = vcat(zero(T) * x, y)
#        @test all(val.(z) .≈ [0, 0, 0, 0, 0, 10, 0, 0, 0])
#        @test length(z.nzcoo) == nnz(y)
#
#        x = sparse([(1, 1), (1, 2), (2, 2)], ones(T, 3), 2, 2)
#        y = sparse([(3, 1)], ones(T, 1), 3, 3)
#        z = cat(x, y; dims = (1, 2))
#
#        @test z isa AbstractSparseArray
#        @test size(z) == (size(x, 1) + size(y, 1), size(x, 2) + size(y, 2))
#        @test all(val.(z) .== [1 1 0 0 0;
#                               0 1 0 0 0;
#                               0 0 0 0 0;
#                               0 0 0 0 0;
#                               0 0 1 0 0])
#
#
#    end
#
#    @testset "collapse" begin
#        x = collapse(sparsevec([1, 1], ones(T, 2), 2))
#        @test all(val.(x) .≈ [val(2*one(T)), 0])
#
#        x = collapse(sparse([(1, 1), (1, 1)], ones(T, 2), 2, 2))
#        @test all(val.(x) .≈ [val(2*one(T)) 0; 0 0])
#    end
#
#    @testset "copy" begin
#        x = sparsevec([1, 1], ones(T, 2), 2)
#        cpx = copy(x)
#        @test all(val.(x) .≈ val.(cpx))
#
#        x = sparse([(1, 1), (1, 1)], ones(T, 2), 2, 2)
#        cpx = copy(x)
#        @test all(val.(x) .≈ val.(cpx))
#    end
#
#    @testset "dropdims" begin
#        x = sparse([(1, 2), (4, 1)], ones(T, 2))
#        y = dropdims(x; dims = 1)
#        @test all(y.nzcoo .== [2; 1;])
#        y = dropdims(x; dims = (1,))
#        @test all(y.nzcoo .== [2; 1])
#        y = dropdims(x; dims = 2)
#        @test all(y.nzcoo .== [1; 4])
#        y = dropdims(x; dims = (2,))
#        @test all(y.nzcoo .== [1; 4])
#    end
#
#
#    @testset "sum" begin
#        x = sparsevec([2, 4], ones(T, 2))
#        @test val(sum(x)) ≈ val(2*one(T))
#
#        x = sparse([(1, 2), (4, 1)], ones(T, 2))
#        @test val(sum(x)) ≈ val(2*one(T))
#
#        x = sparse([(1, 2), (4, 1)], ones(T, 2))
#        @test all(sum(x, dims = (1,)).nzcoo .== [1 2; 1 1])
#
#        @test val(sum(ones(T, 4))) ≈ val(4*one(T))
#        @test val(sum(ones(T, 2, 2))) ≈ val(4*one(T))
#    end
#
#    @testset "permutedims" begin
#        x = sparse([(1, 2), (2, 1)], T[1, 2])
#        y = permutedims(x, (2, 1))
#        @test all(y .≈ T[0 2; 1 0])
#    end
#end

#@testset verbose=true failfast=true "linalg" begin
#    T = ProbSemiring{Float32}
#
#    @testset "transpose" begin
#        x = sparse([(2, 1), (3, 3)], ones(T, 2), 3, 4)
#        y = sparse([(1, 2), (3, 3)], ones(T, 2), 4, 3)
#        @test all(val.(copy(transpose(x))) .≈ val.(y))
#    end
#
#
#    @testset "scalar multiplication" begin
#        x = sparsevec([2, 3], ones(T, 2), 4)
#        @test all(val.(T(3) * x ) .≈ [0, 3, 3, 0])
#        @test all(val.(x * T(3)) .≈ [0, 3, 3, 0])
#
#        @test nnz(zero(T) * x) == 0
#        @test nnz(x * zero(T)) == 0
#
#        x = sparse([(2, 1), (3, 3)], ones(T, 2), 3, 4)
#        @test all(val.(T(3) * x ) .≈ [0 0 0 0;
#                                      3 0 0 0;
#                                      0 0 3 0])
#        @test all(val.(x * T(3) ) .≈ [0 0 0 0;
#                                      3 0 0 0;
#                                      0 0 3 0])
#
#        @test nnz(zero(T) * x) == 0
#        @test nnz(x * zero(T)) == 0
#    end
#
#    @testset "inner product" begin
#        x = sparsevec([2, 3], ones(T, 2), 4)
#        @test val(dot(x, ones(T, 4))) ≈ nnz(x)
#        @test val(dot(ones(T, 4), x)) ≈ nnz(x)
#        @test val(dot(Array(x), ones(T, 4))) ≈ nnz(x)
#    end
#
#    @testset "outer product" begin
#        x = sparsevec([2, 3], ones(T, 2), 4)
#        y = sparsevec([1, 2, 3], T[1, 2, 3], 3)
#        z = x * transpose(y)
#        @test size(z) == (4, 3)
#        @test all(val.(z) .≈ [0 0 0;
#                              1 2 3;
#                              1 2 3;
#                              0 0 0])
#
#        z = Array(x) * transpose(Array(y))
#        @test all(val.(z) .≈ [0 0 0;
#                              1 2 3;
#                              1 2 3;
#                              0 0 0])
#    end
#
#    @testset "element wise addition" begin
#        x = sparsevec([2, 3], ones(T, 2), 4)
#        y = sparsevec([1, 2, 3], 2 .* ones(T, 3), 4)
#        z = x + y
#        @test all(val.(z) .≈ [2, 3, 3, 0])
#        @test all(val.(y + ones(T, 4)) .≈ [3, 3, 3, 1])
#        @test all(val.(ones(T, 4) + y) .≈ [3, 3, 3, 1])
#
#        z = Array(x) + Array(y)
#        @test all(val.(z) .≈ [2, 3, 3, 0])
#        @test all(val.(Array(y) + ones(T, 4)) .≈ [3, 3, 3, 1])
#        @test all(val.(ones(T, 4) + Array(y)) .≈ [3, 3, 3, 1])
#
#        x = sparse([(1, 2), (2, 1)], ones(T, 2), 2, 2)
#        y = sparse([(1, 1), (1, 2), (2, 1)], 2 .* ones(T, 3), 2, 2)
#        z = x + y
#        @test all(val.(z) .≈ [2 3; 3 0])
#        @test all(val.(y + ones(T, 2, 2)) .≈ [3 3; 3 1])
#        @test all(val.(ones(T, 2, 2) + y) .≈ [3 3; 3 1])
#
#        z = Array(x) + Array(y)
#        @test all(val.(z) .≈ [2 3; 3 0])
#        @test all(val.(Array(y) + ones(T, 2, 2)) .≈ [3 3; 3 1])
#        @test all(val.(ones(T, 2, 2) + Array(y)) .≈ [3 3; 3 1])
#    end
#
#    @testset "matrix - vector multiplication" begin
#        A = sparse([(1, 1), (1, 2), (2, 1)], T[1, 2, 3], 2, 2)
#        x = Array(sparsevec([1, 2], T[10, 20], 2))
#
#        y = A * x
#        @test all(val.(y) .≈ [50, 30])
#        y = Array(A) * Array(x)
#        @test all(val.(y) .≈ [50, 30])
#
#        y = transpose(A) * x
#        @test all(val.(y) .≈ [70, 20])
#        y = transpose(Array(A)) * Array(x)
#        @test all(val.(y) .≈ [70, 20])
#
#        y = transpose(x) * A
#        @test all(val.(y) .≈ transpose([70, 20]))
#        y = transpose(Array(x)) * Array(A)
#        @test all(val.(y) .≈ transpose([70, 20]))
#
#        y = transpose(x) * transpose(A)
#        @test all(val.(y) .≈ transpose([50, 30]))
#        y = transpose(Array(x)) * transpose(Array(A))
#        @test all(val.(y) .≈ transpose([50, 30]))
#    end
#
#
#    @testset "kronecker product" begin
#        x = sparsevec([1, 2], T[10, 20], 3)
#        y = kron(x, x)
#        @test all(val.(y) .≈ [100, 200, 0, 200, 400, 0, 0, 0, 0])
#        y = kron(Array(x), Array(x))
#        @test all(val.(y) .≈ [100, 200, 0, 200, 400, 0, 0, 0, 0])
#
#        A = sparse([(1, 1), (1, 2), (2, 1)], T[1, 2, 3], 2, 2)
#        B = kron(A, A)
#        @test all(val.(B) .≈ [1 2 2 4;
#                              3 0 6 0;
#                              3 6 0 0;
#                              9 0 0 0])
#        B = kron(Array(A), Array(A))
#        @test all(val.(B) .≈ [1 2 2 4;
#                              3 0 6 0;
#                              3 6 0 0;
#                              9 0 0 0])
#    end
#
#    @testset "nullspace" begin
#        M = sparse([(1, 1), (3, 1)], ones(T, 2), 3, 3)
#        N = nullspace(M)
#        @test all(val.(N) .≈ [0 0;
#                              1 0;
#                              0 1])
#        N = nullspace(M; keepdims = true)
#        @test all(val.(N) .≈ [0 0 0;
#                              1 0 0;
#                              0 1 0])
#
#        N = nullspace(Array(M))
#        @test all(val.(N) .≈ [0 0;
#                              1 0;
#                              0 1])
#        N = nullspace(Array(M); keepdims = true)
#        @test all(val.(N) .≈ [0 0 0;
#                              1 0 0;
#                              0 1 0])
#    end
#end


#@testset verbose=true failfast=true "semirings" begin
#    T = ProbSemiring{Float32}
#
#    @testset "matrix semiring" begin
#        x = sparse([(1, 1), (3, 1)], ones(T, 2), 3, 3)
#
#        X = MatrixSemiring(x)
#        @test all(val.(val(one(X))) .≈ [1 0 0; 0 1 0; 0 0 1])
#        @test all(val.(val(zero(X))) .≈ [0 0 0; 0 0 0; 0 0 0])
#
#        @test all(val.(val(X * X)) .≈ val.(x * x))
#        @test all(val.(val(X + X)) .≈ val.(x + x))
#
#        @test all(val.(val(X ⊗ X)) .≈ val.(kron(x, x)))
#        @test all(val.(val(X ⊕ X)) .≈ val.(cat(x, x; dims = (1, 2))))
#
#        @test all(val.(val(transpose(X))) .≈ val.(transpose(x)))
#
#
#        Tm = MatrixSemiring{3,SparseMatrix{T}}
#        @test all(val(zero(Tm)) .≈ T[0 0 0; 0 0 0; 0 0 0])
#        @test all(val(one(Tm)) .≈ T[1 0 0; 0 1 0; 0 0 1])
#
#        # Test type preservation
#        Tm = MatrixSemiring{1,SparseArray{T,2,Vector{Dims{2}},Vector{T}}}
#        A = sparse(Dims{2}[], Tm[], 4, 19)
#        B = sparse([(1, 1)], X, 4, 19)
#        C = A .⊕ B
#        @test eltype(C) <: MatrixSemiring{4,<:SparseArray{T,2}}
#    end
#
#    @testset "sparse array of matrix semiring" begin
#        x = sparse([(1, 1), (3, 1)], ones(T, 2), 3, 3)
#        y = sparse([(1, 2), (2, 1)], ones(T, 2), 2, 2)
#        X = sparse([(1,1), (2,2)], MatrixSemiring.([x, x]), 2, 2)
#        Y = sparse([(1,1), (2,2)], MatrixSemiring.([x, x]), 2, 2)
#
#        Z = X .⊕ Y
#        @test nnz(Z) == nnz(X) + nnz(Y)
#
#        Z = transpose.(X)
#        @test all(val.(val(Z[1,1])) .≈ val.(transpose(x)))
#
#        Z = copy(transpose(X .⊕ Y))
#        @test nnz(Z) == nnz(X) + nnz(Y)
#    end
#end

